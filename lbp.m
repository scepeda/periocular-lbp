function [result,vector]=lbp(source,radius,neighbors,nx,ny)
%[result,vector]=lbp(im,radius,neighbors,nx,ny);
%im: imagen de entrada a procesar(como matriz)
%radius: radio en el que se comparan los vecinos.
%neighbors: cantidad de vecinos a comparar.
%nx: cantidad de separaciones de la grilla en el eje x.
%ny: cantidad de separaciones de la grilla en el eje y.
%result: imagen resultante de aplicar el paso de umbral comparando con vecinos.
%vector: salida del algoritmo, con histrogramas concatenados.
    result=zeros(size(source,1)-2*radius,size(source,2)-2*radius);
    for i=(radius+1):(size(source,1)-radius)
        for j=(radius+1):(size(source,2)-radius)
            %source(i,j)
            for n=0:neighbors
                x=floor(radius*cos(2*pi*n/neighbors));
                y=floor(radius*sin(2*pi*n/neighbors));
                if(source(i+x,j+y)>source(i,j))
                    result(i-radius,j-radius)=result(i-radius,j-radius)+2^n;
                end
            end
        end
    end
    vector=lbp_grid(result,nx,ny,neighbors);%divide la imagen en un grid,
    %obtiene el histograma de cada celda y lo concatena en 'vector'.
end
