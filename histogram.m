function result=histogram(source,resolution)
    a=hist(source,1:resolution);
    result=sum(a,2);
end
