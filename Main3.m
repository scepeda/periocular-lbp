clear;
close all;
clc;

%% Parametros
ImgFormat = 'tiff';
MaskFormat = 'bmp';
ImgPath     = '../DB/NICE2/Data/Training/EL5206/Images/';
MaskPath    = '../DB/NICE2/Data/Training/EL5206/SegmentedImages/';
radius      =   3;
neighbors   =   8;
nx          =   5;
ny          =   5;
VectorLength= nx*ny*2^(neighbors);

MaxClasses = 5;
TrainImgs = 2;
TestImgs = 1;

DATA = cell(1,1);
%% Obtener Nombre Imagenes
ImgList = dir([ImgPath '*.' ImgFormat]);

%% Leer Imagenes, m�scara y c�lculo de LBP
IMG = cell(length(ImgList), 1);
MASK = cell(length(ImgList), 1);

nTotalTestImg = 0;
nTotalTrainImg = 0;
nTotalClasses = 0;
for i = 1:(length(IMG))
    if(MaxClasses == nTotalClasses)
        break;
    end
    % Obtener im�genes por clase
    ImgListClass = dir([ImgPath 'C' num2str(i) '_*.' ImgFormat]);
    if (isempty(ImgListClass) || length(ImgListClass) < (TrainImgs + TestImgs))
        continue
    else
        disp(['Leyendo Datos de la Clase ' 'C' num2str(i)]);
        DATA{i} = cell(length(ImgListClass),5);
        % Por cada imagen de la clase, guardar la imagen, la mascara
        % binaria, el nombre y el LBP asociado
        nTotalTestImg = nTotalTestImg + TestImgs;
        nTotalTrainImg = nTotalTrainImg + TrainImgs;
        nTotalClasses = nTotalClasses + 1;
        for j = 1:length(ImgListClass)
            % Imagen
            DATA{i}{j, 1}   = imread([ImgPath ImgListClass(j).name]);
            % Mascara
            DATA{i}{j, 2}   = imread([MaskPath ImgListClass(j).name(1:(end-length(ImgFormat))) MaskFormat]);
            % Nombre
            DATA{i}{j, 3}   = ImgListClass(j).name;
            
            Im      = DATA{i}{j, 1};
            Mask    = DATA{i}{j, 2};
            Mask = umbral(Mask(:,:,1), 255/2)/255;
            Res = zeros(VectorLength, 3);
            for k = 1:3
                ImAux = Im(:,:,k).*Mask;
                [x, Res(:,k)] = lbp(ImAux, radius, neighbors, nx, ny);
            end
            % LBP
            DATA{i}{j, 4} = Res;
        end
    end
end



%% TRAIN SVM
SVM = cell(2, length(DATA));

Train = NaN(nTotalTrainImg, VectorLength*3);
Groups = NaN(nTotalTrainImg, length(DATA));
k = 0;
% Prepare Data
for i = 1:length(DATA)
    if(isempty(DATA{i}))
        continue
    end
    
    Groups(:,i) = zeros(nTotalTrainImg, 1);
    N = randperm(size(DATA{i}, 1));
    for j = 1:TrainImgs
        % Prepare Training Data
        Train(k+j, :) = reshape(DATA{i}{N(j), 4}, 1, []);
        DATA{i}{N(j), 5} = 'Train';
        
        % Prepare Groups Data
        Groups(k+j, i) = 1;
    end
    k = k + TrainImgs;
end

% Train the SVM's
for i = 1:length(DATA)
    if(isempty(DATA{i}))
        continue
    end
    SVM{1,i} = svmtrain(Train, Groups(:,i)); % Aca se pueden agregar distintas opciones para el kernel del SVM
    
    aux = xor(svmclassify(SVM{1, i}, Train), Groups(:,i));
    SVM{2, i} = sum(aux); % numero de errores en entrenamiento
end

%% Testing

% Generate Test Data
Test = NaN(nTotalTestImg, VectorLength*3);
GT =  NaN(nTotalTestImg, length(DATA));
k = 1;
for i = 1:length(DATA)
    if(isempty(DATA{i}))
        continue
    end
    img = 0;
    for j = 1:(size(DATA{i}))
        if(img == TestImgs)
            break;
        end
        if(strcmp(DATA{i}{j, 5},'Train'))
            continue
        end
        GT(:,i) = zeros(nTotalTestImg, 1);
        % Prepare Test Data
        Test(k, :) = reshape(DATA{i}{j, 4}, 1, []);
        DATA{i}{j, 5} = 'Test';
        
        % Prepare Groups Data
        GT(k, i) = 1;
        k = k+1;
        img = img+1;
    end
    
end
