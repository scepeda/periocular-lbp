function [result]=lbp_grid(source,nx,ny,neighbors)
%[result]=lbp_grid(source,nx,ny,neighbors)
%divide la imagen en un grid,
%obtiene el histograma de cada celda y lo concatena en 'result'.
%source: imagen de entrada a procesar(como matriz)
%nx: cantidad de separaciones de la grilla en el eje x.
%ny: cantidad de separaciones de la grilla en el eje y.
%result: salida del algoritmo, con histrogramas concatenados.
    wy=floor(size(source,1)/ny);
    wx=floor(size(source,2)/nx);
    x=1;
    result=zeros(nx*ny*2^neighbors,1);
    c=1;
    while(x+wx<size(source,2))
        y=1;
        while(y+wy<size(source,1))
            result(c:255+c)=histogram(source(y:uint32(round(y+wy)),x:uint32(round(x+wx))),2^neighbors);
            c=c+256;
            y=y+wy;
        end
        x=x+wx;
    end
end
