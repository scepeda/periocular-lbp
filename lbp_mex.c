#include <math.h>
#include <matrix.h>
#include <mex.h>
/**
%im: imagen de entrada a procesar(como matriz)
%radius: radio en el que se comparan los vecinos.
%neighbors: cantidad de vecinos a comparar.
%nx: cantidad de separaciones de la grilla en el eje x.
%ny: cantidad de separaciones de la grilla en el eje y.
%result: imagen resultante de aplicar el paso de umbral comparando con vecinos.
*/

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
	/* Check the number of arguments */
	if(nrhs < 3||nrhs > 3)
	{
		mexErrMsgTxt("Wrong number of input arguments.");
	}
	else if(nlhs > 1)
	{
		mexErrMsgTxt("Too many output arguments.");
	}
	
	/* Macros for the ouput and input arguments */
	/* Outputs*/
	#define result_p plhs[0]
	/* Inputs*/
	#define source_p prhs[0]
	#define radius_p prhs[1]
	#define neighbours_p prhs[2]
	
	mwSize M = mxGetM(source_p);
	mwSize N = mxGetN(source_p);
	mexPrintf("%d-%d\n",M,N);
	double *source;
	source = (double *)mxGetPr(prhs[0]);//source_p
	mexPrintf("elementos:%d\n",mxGetNumberOfElements(prhs[0]));
	
	int radius = mxGetScalar(radius_p);
	int neighbors = mxGetScalar(neighbours_p);
	
	result_p = mxCreateDoubleMatrix(M-2*radius,N-2*radius, mxREAL);
	double *result = mxGetPr(result_p);
	int i,j,n;
	mexPrintf("radio:%d\n",radius);
	mexPrintf("neighbours:%d\n",neighbors);
	int c=0;
	for(i=0;i<(M-1);i++)
	{
        for(j=0;j<(N-1);j++)
		{
			mexPrintf("%d\n",source[i+j*N]);
		}
	}
	/*
	for(i=(radius);i<(M-radius-1);i++)
	{
        for(j=(radius);j<(N-radius-1);j++)
		{
			
			//mexPrintf("%d,%d,%d\n",i,j,(int)source[i+j*N]);
            for(n=0;n<=neighbors;n++)
			{
                int x=floor(radius*cos(2*M_PI*n/neighbors));
                int y=floor(radius*sin(2*M_PI*n/neighbors));
				
                if(source[(i+x)+(j+y)*N]>source[i+j*N])
				{
                    result[(i-radius)+(j-radius)*N]+=2^n;
                }
            }
        }
    }*/
	//liberar memoria source
}
