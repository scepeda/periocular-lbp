function z=umbral(a,u)
%umbral de una imagen a, con valor u: a(i,j)>u=>a(i,j)=255, 0 en caso
%contrario
    z=a;
    for i=1:size(a,1)
        for j=1:size(a,2)
            if a(i,j)>u
                z(i,j)=255;
            else
                z(i,j)=0;
            end
        end
    end
end
